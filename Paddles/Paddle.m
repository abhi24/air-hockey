
#import "Paddle.h"

@implementation Paddle

@synthesize touch;
@synthesize speed;
@synthesize maxSpeed;

- (id)initWithView:(UIView *)paddle Boundary:(CGRect)rect MaxSpeed:(float)max
{
    self = [super init];
    if (self)
    {
        view = paddle;
        boundary = rect;
        maxSpeed = max;
    }
    return self;
}

- (void)reset
{
    pos.x = boundary.origin.x + boundary.size.width / 2;
    pos.y = boundary.origin.y + boundary.size.height / 2;
    view.center = pos;
}

- (void)move:(CGPoint)pt
{
    if (pt.x < boundary.origin.x)
    {
        pt.x = boundary.origin.x;
    }
    else if (pt.x > boundary.origin.x + boundary.size.width)
    {
        pt.x = boundary.origin.x + boundary.size.width;
    }
    if (pt.y < boundary.origin.y)
    {
        pt.y = boundary.origin.y;
    }
    else if (pt.y > boundary.origin.y + boundary.size.height)
    {
        pt.y = boundary.origin.y + boundary.size.height;
    }
    pos = pt;
}

- (CGPoint)center
{
    return view.center;
}
- (BOOL)intersects:(CGRect)rect
{
    return CGRectIntersectsRect(view.frame, rect);
}

- (float)distance:(CGPoint)pt
{
    float diffx = view.center.x - pt.x;
    float diffy = view.center.y - pt.y;
    return sqrtf(diffx * diffx + diffy * diffy);
}

- (void)animate
{
    if (CGPointEqualToPoint(view.center, pos) == FALSE)
    {
        float d = [self distance:pos];
        
        if (d > maxSpeed)
        {
            float r = atan2(pos.y - view.center.y, pos.x - view.center.x);
            float x = view.center.x + cos(r) * maxSpeed;
            float y = view.center.y + sin(r) * maxSpeed;
            view.center = CGPointMake(x, y);
            speed = maxSpeed;
        }
        else
        {
            view.center = pos;
            speed = d;
        }
    } else speed = 0;   
}



@end
