//
//  PaddlesAppDelegate.m
//  Paddles
//
//  Created by Abhishek Gupta on 4/16/17.
//  Copyright © 2017 Abhishek Gupta. All rights reserved.
//

#import "PaddlesAppDelegate.h"
#import "PaddlesViewController.h"
#import "TitleViewController.h"
int i=0;

@implementation PaddlesAppDelegate

- (void)showTitle
{
    if (self.gameController)
    {
        [self.viewController dismissViewControllerAnimated:NO completion:nil];
        self.gameController = nil;
    }
}

- (void)playGame:(int)computer
{

    if (self.gameController == nil)
    {
        self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        self.viewController = [storyboard instantiateViewControllerWithIdentifier:@"TitleViewController"];
        self.window.rootViewController = self.viewController;
        [self.window makeKeyAndVisible];

        self.gameController = [storyboard instantiateViewControllerWithIdentifier:@"PaddlesViewController"];
        self.gameController.computer = computer;
        [self.viewController presentViewController:self.gameController animated:NO completion:nil];
    }
    
    else
    {
        self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        self.viewController = [storyboard instantiateViewControllerWithIdentifier:@"TitleViewController"];
        self.window.rootViewController = self.viewController;
        [self.window makeKeyAndVisible];
        self.gameController = [storyboard instantiateViewControllerWithIdentifier:@"PaddlesViewController"];
        self.gameController.computer = computer;
        [self.viewController presentViewController:self.gameController animated:NO completion:nil];

    }
}




- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    self.viewController = [storyboard instantiateViewControllerWithIdentifier:@"TitleViewController"];
    self.window.rootViewController = self.viewController;
    [self.window makeKeyAndVisible];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    if (self.gameController)
        [self.gameController pause];
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    if (self.gameController)
        [self.gameController resume];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
}

@end
