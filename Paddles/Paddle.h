#import <Foundation/Foundation.h>

@interface Paddle : NSObject
{
    UIView *view;
    CGRect boundary;
    CGPoint pos;
    float maxSpeed;
    float speed;
    __unsafe_unretained UITouch *touch;
}
@property (assign, nonatomic) UITouch *touch;
@property (readonly, nonatomic) float speed;
@property (assign, nonatomic) float maxSpeed;


- (id)initWithView:(UIView *)paddle Boundary:(CGRect)rect MaxSpeed:(float)max;


- (void)reset;

- (void)move:(CGPoint)pt;
- (CGPoint)center;
- (BOOL)intersects:(CGRect)rect;

- (float)distance:(CGPoint)pt;

- (void)animate;

@end
