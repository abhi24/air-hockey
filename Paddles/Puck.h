
#import <Foundation/Foundation.h>
#import "Paddle.h"

@interface Puck : NSObject
{
    UIView *view;
    CGRect rect[3];       int box;                float maxSpeed;
    float speed;
    float dx, dy;
    int winner;
    
}

@property (readonly, nonatomic) float maxSpeed;
@property (readonly, nonatomic) float speed;
@property (readonly, nonatomic) float dx;
@property (readonly, nonatomic) float dy;
@property (readonly, nonatomic) int winner;


- (id)initWithPuck:(UIView *)puck
          Boundary:(CGRect)boundary
             Goal1:(CGRect)goal1
             Goal2:(CGRect)goal2
          MaxSpeed:(float)max;

- (void)reset;

- (CGPoint)center;

- (BOOL)animate;

- (BOOL)handleCollision:(Paddle *)paddle;

@end

























