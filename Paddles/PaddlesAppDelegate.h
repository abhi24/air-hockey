//
//  PaddlesAppDelegate.h
//  Paddles
//
//  Created by Abhishek Gupta on 4/16/17.
//  Copyright © 2017 Abhishek Gupta. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PaddlesViewController;
@class TitleViewController;

@interface PaddlesAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) PaddlesViewController *gameController;
@property (strong, nonatomic) TitleViewController *viewController;

- (void)showTitle;
- (void)playGame:(int)computer;



@end
