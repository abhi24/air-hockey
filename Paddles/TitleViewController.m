//
//  TitleViewController.m
//  AirHockey
//
//  Created by Abhishek Gupta on 4/18/17.
//  Copyright © 2017 Abhishek Gupta. All rights reserved.
//

#import "TitleViewController.h"
#import "PaddlesAppDelegate.h"

@interface TitleViewController ()

@end

@implementation TitleViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}
- (IBAction)onPlay:(id)sender
{
    PaddlesAppDelegate *app = (PaddlesAppDelegate *)[UIApplication sharedApplication].delegate;
    UIButton *button = (UIButton *)sender;
    [app playGame:(int)button.tag];
}





@end
