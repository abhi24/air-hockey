//
//  main.m
//  Paddles
//
//  Created by Abhishek Gupta on 4/16/17.
//  Copyright © 2017 Abhishek Gupta. All rights reserved.
//
#import <UIKit/UIKit.h>

#import "PaddlesAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([PaddlesAppDelegate class]));
    }
}

